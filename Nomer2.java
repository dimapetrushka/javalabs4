package JavaLabs4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 25.02.2016.
 * Создать метод, который будет выводить указанный массив на экран в строку. С помощью созданного метода и метода из предыдущей задачи
 * заполнить 5 массивов из 10 элементов каждый случайными числами и вывести все 5 массивов на экран, каждый на отдельной строке.
 */
public class Nomer2 {
    public static void vivod(int[] a) {
        for (int i = 0; i < 10; i++) {
            System.out.print(a[i]+"\t");
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
        int[] mas = new int[10];
        //считывание с клавиатуры
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        String a1 = buf.readLine(), b1 = buf.readLine();
        //ловим ошибку
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
        } catch (Exception e) {
            System.out.println("Введено не int");
            return;
        }
        int a = Integer.parseInt(a1), b = Integer.parseInt(b1);
        //заполнение пяти массивов через два метода
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                mas[j]=Nomer1.rand(a, b);
            }
            vivod(mas);
        }
    }
}
