package JavaLabs4;

/**
 * Created by Dima1 on 02.03.2016.
 * отсортировать данный массив по возрастанию любым способом и вывести его на экран
 */
public class Nomer3 {
    public static void sort(int[] mas) {
        //сортировка пузырьком
        for (int i = 0; i < mas.length - 1; i++) {
            for (int j = i + 1; j < mas.length; j++) {
                if (mas[i] > mas[j]) {
                    int c = mas[i];
                    mas[i] = mas[j];
                    mas[j] = c;
                }
            }
        }
        //вывод массива
        for (int i = 0; i < 10; i++) {
            System.out.print(mas[i] + "\t");
        }
    }

    public static void main(String[] args) {
        int[] mas = new int[10];
        //заполнение массива
        for (int i = 0; i < 10; i++) {
            mas[i] = (int) (Math.random() * 11);
        }
        //вызов метода сортировки массива
        sort(mas);
    }
}
