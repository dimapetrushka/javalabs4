package JavaLabs4;

/**
 * Created by Dima1 on 02.03.2016.
 * написать рекурсивный метод считающий числа фибоначчи
 * Прекратить считать числа,если их подсчёт длится более 1 минуты
 */
public class Rekursia {
    static int fib(int n) {
        //метод вычисления числа Фибоначчи через рекурсию
        if ((n == 1) || (n == 2)) {
            return 1;
        } else return fib(n - 2) + fib(n - 1);
    }

    public static void main(String[] args) {
        int n = 0;
        //переменная для засечения времени
        int ms = 0;
        //считаем до 60000,потому что в минуте 60000 мс
        while (ms <= 60000) {
            //увеличение числа Фибоначчи
            n++;
            //засекания начального времени
            long start = System.currentTimeMillis();
            //подсчёт самого числа фибоначчи функции
            fib(n);
            //засекание конечного времени
            long end = System.currentTimeMillis();
            //сравнение времени
            ms = (int) (end - start);
        }
        System.out.println("С " + n + "-ого числа фибоначчи, его значение вычисляется 1 минуту и более");

    }
}
