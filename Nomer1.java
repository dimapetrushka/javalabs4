package JavaLabs4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dima1 on 25.02.2016.
 * создать статический метод,который принимает два целых числа(a,b) и генерирует случайное число в промежутке [a,b];
 * создать массив из 20 таких чисел;
 */
public class Nomer1 {
    public static int rand(int a, int b) {
       //int b = 0;
        // если первая граница больше второй, то меняем их местами
        if (a > b) {
            int c = a;
            a = b;
            b = c;
        }
        //выравниваем диапозон значений,чтобы нормально выводилось
        if (a < 0) b = b + Math.abs(a)+1;
        int c = (int) (Math.random() * b + a);
        return c;
    }

    public static void main(String[] args) throws IOException {
        //объявляем массив
        int[] mas = new int[20];
        //считывание с клавиатуры
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        String a1 = buf.readLine(), b1 = buf.readLine();
        //ловим ошибку
        try {
            Integer.parseInt(a1);
            Integer.parseInt(b1);
        } catch (Exception e) {
            System.out.println("Введено не int");
            return;
        }
        int a = Integer.parseInt(a1), b = Integer.parseInt(b1);
        //заполнение массива рандомными числами с объявленными границами через метод rand, и вывод
        for (int i = 0; i < 20; i++) {
            mas[i] = rand(a, b);
            System.out.print(mas[i] + "\t");
        }
    }
}
